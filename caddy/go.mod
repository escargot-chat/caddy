module caddy

go 1.14

require github.com/caddyserver/caddy/v2 v2.3.0-rc2

replace github.com/caddyserver/caddy/v2 => ../
