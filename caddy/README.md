## Build

Rebase onto the latest stable tag.
Change the commit identifier in `go.mod` to the tag you rebased on.

```
GOOS=linux GOARCH=amd64 go build
```

## Test

First, build for current system (`go build`), then:

```
./caddy.exe version
./caddy.exe run
```

It shouldn't give an error.
